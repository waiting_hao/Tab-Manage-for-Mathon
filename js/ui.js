﻿function colorToRgb(color, alpha) {
    if(typeof color === "string") {
        // 如果是16进制颜色
        if(color.indexOf("#") === 0) {
            var r, g, b;
            r = color.substr(1, 2);
            g = color.substr(3, 2);
            b = color.substr(5, 2);

            //16进制转成10进制
            r = parseInt(r, 16).toString(10);
            g = parseInt(g, 16).toString(10);
            b = parseInt(b, 16).toString(10);

            if(alpha === undefined) {
                return "rgb(" + r + "," + g + "," + b + ")";
            } else {
                if(alpha === true) {
                    return "rgba(" + r + "," + g + "," + b + ", 1)";
                } else {
                    return "rgba(" + r + "," + g + "," + b + "," + alpha + ")";
                }
            }
        }
        if(color.indexOf("rgb") === 0) {
            return color;
        }
    }
    return false;
}
/**
  * 行类
  *
  */
function TabRowUi(option) {
    if(typeof option == 'undefined') {
        throw 'TabRowUi: option参数为必须参数';
    }
    this.el = null;
    this.id = option.id;

    if(typeof TabRowUi._initialized == "undefined") {
        TabRowUi.prototype.setText = function(text) {
            var row = document.getElementById(TabManageUi.rowIdPrefix + this.id);
            var p = row.getElementsByTagName('p');
            p[0].textContent = text;
        }
    }
    TabManageUi._initialized = true;
}

/**
  *   option
  *     width  行宽
  *
  */
function TabManageUi(divId, option) {
    var me = this;

    if(typeof option == "object") {
        TabManageUi.width = option.width || 300;
        TabManageUi.fontColor = option.fontColor || "#ffffff";
        TabManageUi.borderColor = option.borderColor || "#000000";
        TabManageUi.closeColor = option.closeColor || "#000000";
        TabManageUi.backgroundUrl = option.backgroundUrl || "../image/body_bg_new.jpg";
        TabManageUi.rowBackgroundUrl = option.rowBackgroundColor;
    }

    var body = document.getElementById('body-id');
    body.style.backgroundImage = "url(" + TabManageUi.backgroundUrl + ")";
    
    // 定义私有变量
    var wrapDivElement = document.getElementById(divId);
    var tabListDiv = document.createElement("div");
    var _rows = [];

    // 定义公有变量
    this.id = '';
    this.el = tabListDiv;



    if(typeof TabManageUi._initialized == "undefined") {
        // 定义静态变量
        TabManageUi.idSeed = 0;
        TabManageUi.rowIdPrefix = "tab-row-";
        TabManageUi.listDivIdPrefix = "tab-list-";
        TabManageUi.tabCloseButtonIdPrefix = "close-button-";
        TabManageUi.currentRowId = null;

        /** 更加行
          *
          * option
          *     text: 列上显示的文字
          *     id:   唯一标识
          */
        TabManageUi.prototype.appendRow = function(option) {
            if(typeof option == 'undefined') {
                throw 'option参数为必须参数';
                return;
            }
            // 生成id
            this.id = option.id || ("auto-" + TabManageUi.idSeed++);
            
            if(typeof option.image != 'undefined') {
                var iconImg = document.createElement('img');
                iconImg.style.width = 14;
                iconImg.style.height = 14;
                iconImg.style.margin = "0 5 0 5";
                iconImg.src = option.image; 
            }
            

            // 创建文字
            var textNode = document.createTextNode(option.text);
            // 创建P元素并添加文字
            var displayText = document.createElement("p");
            displayText.className = "row-p";
            displayText.appendChild(textNode);
            displayText.style.color = TabManageUi.fontColor;

            // 关闭按钮
            var closeButton = document.createElement("a");
            closeButton.id = TabManageUi.tabCloseButtonIdPrefix + this.id;
            closeButton.className = "row-close";
            closeButton.href = "#";
            closeButton.style.color = TabManageUi.closeColor;
            closeButton.onmouseover = function() {
                closeButton.style.color = "white";
                closeButton.style.backgroundColor = colorToRgb(TabManageUi.rowBackgroundUrl);
            };
            closeButton.onmouseout = function() {
                closeButton.style.color = TabManageUi.closeColor;
                closeButton.style.backgroundColor = colorToRgb(TabManageUi.rowBackgroundUrl, 0);
            };

            var xText = document.createTextNode("X");
            closeButton.appendChild(xText);

            // 把P加到一行中
            var rowDiv = document.createElement("div");
            rowDiv.id = TabManageUi.rowIdPrefix + this.id;
            rowDiv.className = "row";
            rowDiv.style.borderColor = TabManageUi.borderColor;
            rowDiv.style.background = colorToRgb(TabManageUi.rowBackgroundUrl, 0.3);
            //rowDiv.style.width = TabManageUi.width;
            if(typeof iconImg != 'undefined') {
                rowDiv.appendChild(iconImg);
            }
            rowDiv.appendChild(displayText);
            rowDiv.appendChild(closeButton);

            // 保存行对象
            var rowOption = {};
            rowOption.id = this.id;
            _rows.push(new TabRowUi(rowOption));

            tabListDiv.appendChild(rowDiv);
        };

        TabManageUi.prototype.activateRow = function(rowId) {
            var row = document.getElementById(TabManageUi.rowIdPrefix + rowId);
            var currentRow = document.getElementById(TabManageUi.rowIdPrefix + TabManageUi.currentRowId);
            if(currentRow) {
                currentRow.className = currentRow.className.replace(/ activate-row/, "");
            }
            
            if(row)
            {
                row.className += " activate-row";
                TabManageUi.currentRowId = rowId;
            }
        };

        // 获取行对象
        TabManageUi.prototype.getRows = function()
        {
            return _rows;
        }

        // 获取行对象
        TabManageUi.prototype.getRowById = function(id)
        {
            for (var count = _rows.length - 1; count >= 0; count--) {
                
                var row = _rows[count];
                if(row.id == id) {
                    return row;
                }
            };
        }

        TabManageUi.prototype.on = function(event, cbfunction) {
            switch(event)
            {
                /** 行单击事件
                  *
                  * itemclick(tabManageUi, row, el)
                  *         tabManageUi:tab列表对象
                  *         row:        被点击的行对象
                  *         el:         行的DOM对象
                  */
                case 'itemclick' :
                    if(typeof cbfunction == 'function') {
                        appendItemClickEven(cbfunction);
                    }
                    break;
                case 'itemclose' :
                    if(typeof cbfunction == 'function') {
                        appendItemCloseEven(cbfunction);
                    }
                    break;
                default:
                    throw "TabManageUi: 不存在"+event+"事件";
            }
        };

        TabManageUi.prototype.removeRow = function(rowId) {
            var row = document.getElementById(TabManageUi.rowIdPrefix + rowId);
            if(row) {
                tabListDiv.removeChild(row);
            }
            else
            {
                throw "TabManageUi: 不存在此ID";
            }
            
        }

        // 私有函数定义
        function appendItemClickEven(cbfunction) {

            var rowsLength = _rows.length;
                var rowEls = tabListDiv.children;
            for (var count = _rows.length - 1; count >= 0; count--) {
                
                var row = _rows[count];
                var rowEl = rowEls[count];

                rowEl.onclick = function() {
                    var id = this.id.substring(TabManageUi.rowIdPrefix.length, this.id.length);
                    cbfunction(me, me.getRowById(id), this, arguments);
                    me.activateRow(id);
                }
            };
        }

        function appendItemCloseEven(cbfunction) {
            var rowsLength = _rows.length;
            for (var count = _rows.length - 1; count >= 0; count--) {
                
                var closeEls = tabListDiv.getElementsByClassName('row-close');
                var closeEl = closeEls[count];

                closeEl.onclick = function() {
                    var id = this.id.substring(TabManageUi.tabCloseButtonIdPrefix.length, this.id.length);
                    cbfunction(me, me.getRowById(id), this, arguments);                    
                }
            };
        }
    }
    TabManageUi._initialized = true;
    tabListDiv.id = TabManageUi.listDivIdPrefix + TabManageUi.idSeed++;
    wrapDivElement.appendChild(tabListDiv);
}
