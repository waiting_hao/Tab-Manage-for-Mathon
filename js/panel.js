﻿var refreshEvent = "reloadTab";

var rt = window.external.mxGetRuntime();
var browser = rt.create("mx.browser");
rt.onAppEvent = function(obj) {
        if(obj.type == "ACTION_SHOW") {
            location.reload();
        }
    }
rt.listen(refreshEvent, function() {
	location.reload();
});

function load() {
	var option = new TabOption();

	var uiOption = {};
	uiOption.width = "90%";
	uiOption.fontColor = option.getFontColor();
	uiOption.borderColor = option.getBorderColor();
	uiOption.closeColor = option.getCloseButtonColor();
	uiOption.backgroundUrl = option.getBackgroundUrl();
	uiOption.rowBackgroundColor = option.getRowBackgroundColor();
	var tabList = new TabManageUi("tabsDivId", uiOption);

	var tabNu = browser.tabs.length;
	for(var count = 0; count < tabNu; count++) {
		var tab = browser.tabs.getTab(count);
		var option = {},
	    iconUrl = rt.storage.getConfig(getHost(tab.url));
		if (tab.url.indexOf("maxthon.cn") >= 0) {
			iconUrl = "http://www.maxthon.cn/favicon.ico";
		} else if (tab.url.indexOf("about:last") >= 0) {
			iconUrl = "mx://res/last-visit/images/favicon.png";
		} else if (tab.url.indexOf("about:extensions") >= 0) {
			iconUrl = "mx://res/extensions/images/favicon.png";
		} else if (tab.url.indexOf("about:config") >= 0) {
			iconUrl = "mx://res/options/images/favicon.png";
		} else if (tab.url.indexOf("about:favorites") >= 0) {
			iconUrl = "mx://res/favorites/images/favicon.png";
		}
		if (!iconUrl || iconUrl == "") {
			iconUrl = "http://"+getHost(tab.url)+"/favicon.ico";
		}
		//favicon.ico
		option.text = tab.title;
		option.id = tab.id;
		option.image = iconUrl;
		tabList.appendRow(option);
	}
	var currentTab = browser.tabs.getCurrentTab();
	tabList.activateRow(currentTab.id);

	tabList.on('itemclick', function(tabMUI, row, el) {
		var tab = browser.tabs.getTabById(row.id);
		if(tab) {
			tab.activate();
		}
	});

	tabList.on('itemclose', function(tabMUI, row, el) {
		var tab = browser.tabs.getTabById(row.id);
		if(tab) {
			tab.close();
		}
	});

	browser.onBrowserEvent = function(obj) {
		switch(obj.type) {
			case "TAB_SWITCH" :
				if(tabList.getRowById(obj.to)) {
					tabList.activateRow(obj.to);
				}
				break;
			case "PAGE_CLOSED" :
				tabList.removeRow(obj.id);
				break;
			case "PAGE_LOADED" :
        		rt.post(refreshEvent);
				break;
		}
	}
}

var getHost = function(url) { 
        var host = "null";
        if(typeof url == "undefined"
                        || null == url)
                url = window.location.href;
        var regex = /.*\:\/\/([^\/]*).*/;
        var match = url.match(regex);
        if(typeof match != "undefined"
                        && null != match)
                host = match[1];
        return host;
}

var getfHost = function(url) { 
        var host = "null";
        if(typeof url == "undefined"
                        || null == url)
                url = window.location.href;
        var regex = /.*\:\/\/(.*)?([^\/]*).*/;
        var match = url.match(regex);
        if(typeof match != "undefined"
                        && null != match)
                host = match[2];
        return host;
}