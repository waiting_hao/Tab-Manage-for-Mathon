﻿


function TabOption() {
	if(typeof TabOption._initialized == "undefined") {
		// 定义私有变量
		var rt = window.external.mxGetRuntime();

		var backgroundUrl = "bk",
			backgroundType = "bt";
		var fontColor = 'fc';
		var borderColor = 'bc';
		var closeButtonColor = 'cbc';
		var rowBackgroundColor = "rbg";

		/**
		  *	判断是否已经保存过背景图片
		  *
		  **/
		TabOption.prototype.hasBackgroundUrl = function() {
			return rt.storage.getConfig(backgroundUrl) !== '';
		}

		TabOption.prototype.saveBackgroundType = function(type) {
			rt.storage.setConfig(backgroundType, type);
		};

		TabOption.prototype.getBackgroundType = function() {
			return rt.storage.getConfig(backgroundType);
		}

		/**
		  *	保存背景图片Url
		  **/
		TabOption.prototype.saveBackgroundUrl = function(url) {
			if(typeof url === "string")
			{
				rt.storage.setConfig(backgroundUrl, url);
			}
		}
		/**
		  *	获取背景图片Url
		  *
		  * return
		  *		值为空时返回空字符串
		 **/
		TabOption.prototype.getBackgroundUrl = function() {
			var dfdf = rt.storage.getConfig(backgroundUrl);
			return rt.storage.getConfig(backgroundUrl);
		}

		TabOption.prototype.saveFontColor = function(value) {
			rt.storage.setConfig(fontColor, value);
		}

		TabOption.prototype.getFontColor = function() {
			return rt.storage.getConfig(fontColor);
		}

		TabOption.prototype.saveBorderColor = function(value) {
			rt.storage.setConfig(borderColor, value);
		}

		TabOption.prototype.getBorderColor = function() {
			return rt.storage.getConfig(borderColor);
		}

		TabOption.prototype.saveCloseButtonColor = function(value) {
			rt.storage.setConfig(closeButtonColor, value);
		}

		TabOption.prototype.getCloseButtonColor = function() {
			return rt.storage.getConfig(closeButtonColor);
		}

		TabOption.prototype.saveRowBackgroundColor = function(value) {
			rt.storage.setConfig(rowBackgroundColor, value);
		}

		TabOption.prototype.getRowBackgroundColor = function(value) {
			return rt.storage.getConfig(rowBackgroundColor);
		}

	}
	TabOption._initialized = true;
}