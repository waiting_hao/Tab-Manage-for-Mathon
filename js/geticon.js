﻿var rt = window.external.mxGetRuntime(),
    host = location.host,
    iconUrl = rt.storage.getConfig(location.host);
if(!iconUrl || iconUrl == "") {
	iconUrl = document.querySelectorAll("link[rel='shortcut icon']")[0].href;
}
if(iconUrl && iconUrl != "") {
	rt.storage.setConfig(host, iconUrl);
}